## Binary Clock v1.1
Just a JS binary clock

---

## Code Example
```javascript
new BinaryClock( document.getElementById('clock') );
```

---

## Motivation
> Just something fun to gain skillz.

1. Partial drawing, draw only that is needs redrawing
1. Try preloading images
1. Maybe multi-instantiable, for whatever reason.
1. Just wanted try to make something 'canvassy' so I made a binary clock...
