/**
* @author D.C. de Brabander
*
* @description A function to render a binary clock on a HTML canvas :-)
* @version 1.1
*/
var BinaryClock = function( c, d, m ){

    // Normal clock or countdown ?
    this.modes = {
        'clock' : 1,
        'countdown' : -1,   // Not implemented yet
        'static': 0         // Not implemented yet
    };

    //holds the type of clock
    this.mode = this.modes.clock;

    // the canvas/context to work with
    this.canvas = c;
    this.context = null;

    // the date to start with, defaults to now
    this.date = d;
    this.date_layout = ['H','i','s'];

    // Draw speed and interval in ms
    this.fps = 2;
    this.interval = null;

    // Holds the LEDS info
    // such as LED margin and size
    this.leds = {
        width: 40,
        height: 40,
        margin: 5,
        x: 0, y: 0,                                                 // remember for draaw()
        images: { led_on: 'led_on.svg', led_off: 'led_off.svg' }    // these will appear in instance's preloaded images
    };

    // Holds all preloaded images
    this.images = {};

    // Hold previous binary state
    this.prev_state;

    /**
    * Convert date string to binary
    */
    this.start = function(){

        // Clock or Countdown?
        if( typeof this.modes[ m ] !== 'undefined' ) {
            this.mode = m;
            this.interval = this.modes[ m ];
        }

        // Set date if we need to Countdown or something
        if( this.mode === 'countdown' && typeof this.date === 'undefined' ){
            this.date = new Date();
        }

        // Check if canvas is set
        if( typeof this.canvas !== 'undefined' ) {

            // init some stuff
            this.context = this.canvas.getContext( '2d' );
            this.interval = Math.abs( this.mode * (1000/this.fps) );

            // calculate canvas dimensions
            this.setCanvasSize();

            // Preload and start drawing
            this.preloader( this.leds.images, this.draw, this );
        }
    }

    /**
    * Public setter for canvas size
    * Reads LED size from object to calculate total dimensions
    */
    this.setCanvasSize = function(){

        // instance.date_layout is array of the chosen layout of the clock, always per 2 numbers (padded with 0)
        // instance.leds.height * 4 because we are using 4 bits per digit
        this.canvas.width = (this.leds.width * (this.date_layout.length * 2) ) + (this.leds.margin * (this.date_layout.length * 2) );
        this.canvas.height = (this.leds.height * 4) + (this.leds.margin * 4);

        // Trigger full redraw
        this.prev_state = null;
    }

    /**
    * Public setter for LED size
    * Which also calls setCanvasSize to make sure everything shows correctly
    */
    this.setLedSize = function( w, h ){

        // set dimensions
        if( typeof w === "undefined" ){
            return;

        }else if( typeof h === "undefined" ){
            this.leds.width = w;
            this.leds.height = w;

        }else{
            this.leds.width = w;
            this.leds.height = h;
        }

        // update canvas size
        this.setCanvasSize();
    }

    /**
    * Simple preloader function
    * Saves in global images object
    */
    this.preloader = function( sources, callback, instance ){

        var done = 0,
            src = null,
            total = Object.keys( sources ).length;

        for( var img in sources ){

            if( !sources.hasOwnProperty(img) ) continue;

            instance.images[img] = new Image();
            instance.images[img].onload = function( e ){

                if( ++done === total ){
                    callback( instance );
                }
            }

            instance.images[img].src = sources[img];
        }
    }

    /**
    * Draw function
    * calls itself when called upon once (after loading and stuff)
    */
    this.draw = function( _this ){

        // Clockmode (mode 1)? refresh date every cycle
        if( _this.mode === 1 ){
            _this.date = new Date();
        }

        // Convert date string to binary code
        var cur_state = _this.getBinaryDate( _this.date_layout, _this );

        // reset X,Y
        _this.x = 0;
        _this.y = 0;

        // For each block of binary (hours, minutes, ...)
        cur_state.forEach( function( block, x ){

            // Loop through the bits
            for( var y in block ){

                // draw when previous state was different or first cycle
                if( !_this.prev_state || ( !!_this.prev_state && _this.prev_state[ x ][ y ] !== block[y] ) ){

                    // First clear that previous state
                    _this.context.clearRect(_this.x, _this.y, _this.leds.width, _this.leds.height);

                    if( block[y] === '1' ){
                        _this.context.drawImage( _this.images.led_on, _this.x, _this.y, _this.leds.width, _this.leds.height );
                    }else{
                        _this.context.drawImage( _this.images.led_off, _this.x, _this.y, _this.leds.width, _this.leds.height );
                    }
                }

                // Update current Y position in canvas
                _this.y += _this.leds.margin + _this.leds.height;
            }

            // reset Y to 0 and go to next column (X)
            _this.y = 0;
            _this.x += _this.leds.margin + _this.leds.width;
        });

        // cache current as previous state
        _this.prev_state = cur_state;

        // how many seconds wait?
        if( _this.interval > 0){
            setTimeout( _this.draw, _this.interval, _this );
        }
    };

    /**
    * Get binary date string from instance's date object
    */
    this.getBinaryDate = function( params ){

        var date_str = '', bin = [], _this = this;

        // Here we can implement needed date functionalities
        // must append numbers (per 2) to date_str
        params.forEach( function( d ){
            switch( d ){
                case 'H':
                    date_str += _this.pad( _this.date.getHours(), 2);
                    break;

                case 'i':
                    date_str += _this.pad( _this.date.getMinutes(), 2);
                    break;

                case 's':
                    date_str += _this.pad( _this.date.getSeconds(), 2);

                default: break;
            }
        });

        // convert date_str to binary digits
        date_str.split("").forEach( function( c ){
            bin.push( _this.pad( _this.toBin(c), 4) );
        });

        return bin;
    };


    /**
    * Converts number to binary
    */
    this.toBin = function( n ){
        return (n >>> 0).toString(2);
    }

    /**
    * Pad strings with zeroes until length 'l' is reached
    */
    this.pad = function( n, l ){

        // convert input to string if needed
        n = typeof n === "string" ? n : ""+n;

        // proceed if given length is bigger current size of 'n'
        if( p = (l - n.length ) ){
            while( p-- ){
                n = "0"+n;
            }
        }
        return n;
    }

    // Start and Draw clock
    this.start();
};
